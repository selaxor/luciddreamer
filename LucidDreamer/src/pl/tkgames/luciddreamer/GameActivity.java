package pl.tkgames.luciddreamer;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.camera.hud.controls.AnalogOnScreenControl;
import org.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.andengine.engine.camera.hud.controls.AnalogOnScreenControl.IAnalogOnScreenControlListener;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;
import org.andengine.util.math.MathUtils;

import android.hardware.SensorManager;
import android.opengl.GLES20;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class GameActivity extends BaseGameActivity
{

	Scene scene;
	SmoothCamera mCamera;
	protected static final int CAMERA_WIDTH = 800;
	protected static final int CAMERA_HEIGHT = 480;
	
	private BitmapTextureAtlas playerBodyTexture;
	private BitmapTextureAtlas playerSwordTexture;
	private ITextureRegion playerBodyTexureRegion;
	private ITextureRegion playerSwordTexureRegion;
	
	private PhysicsWorld physicsWorld;

	private BitmapTextureAtlas mOnScreenControlTexture;
	private ITextureRegion mOnScreenControlBaseTextureRegion;
	private ITextureRegion mOnScreenControlKnobTextureRegion;

	@Override
	public EngineOptions onCreateEngineOptions()
	{

		mCamera = new SmoothCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT, 310, 310, 0);

		EngineOptions options = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), mCamera);

		options.getTouchOptions().setNeedsMultiTouch(true);

		return options;
	}

	@Override
	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws Exception
	{
		// TODO Auto-generated method stub

		loadGfx();
		// resource
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	private void loadGfx()
	{
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		// width and height power of 2^x
		playerBodyTexture = new BitmapTextureAtlas(getTextureManager(), 64, 64);
		playerBodyTexureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(playerBodyTexture, this, "player.png", 0, 0);
		playerBodyTexture.load();
		
		playerSwordTexture = new BitmapTextureAtlas(getTextureManager(), 64, 64);
		playerSwordTexureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(playerBodyTexture, this, "sword.png", 0, 0);
		playerSwordTexture.load();

		this.mOnScreenControlTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 128, TextureOptions.BILINEAR);
		this.mOnScreenControlBaseTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, this, "onscreen_control_base.png", 0, 0);
		this.mOnScreenControlKnobTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, this, "onscreen_control_knob.png", 128, 0);
		this.mOnScreenControlTexture.load();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws Exception
	{
		this.scene = new Scene();
		this.scene.setBackground(new Background(0, 125, 58));
		physicsWorld = new PhysicsWorld(new Vector2(0, 0), false);
		this.scene.registerUpdateHandler(physicsWorld);
		createWalls();

		pOnCreateSceneCallback.onCreateSceneFinished(this.scene);

	}

	private void createWalls()
	{
		// TODO Auto-generated method stub
		FixtureDef WALL_FIX = PhysicsFactory.createFixtureDef(0.0f, 0.0f, 0.0f);
		Rectangle ground = new Rectangle(0, CAMERA_HEIGHT - 15, CAMERA_WIDTH, 15, this.mEngine.getVertexBufferObjectManager());
		ground.setColor(new Color(15, 50, 0));
		PhysicsFactory.createBoxBody(physicsWorld, ground, BodyType.StaticBody, WALL_FIX);
		this.scene.attachChild(ground);
	}

	@Override
	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception
	{

		final Sprite sPlayerBody = new Sprite(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2, playerBodyTexureRegion, this.mEngine.getVertexBufferObjectManager());
		// sPlayer.setRotation(45.0f);
		final Sprite sPlayerSword = new Sprite(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2, playerSwordTexureRegion, this.mEngine.getVertexBufferObjectManager());
		
		this.mCamera.setChaseEntity(sPlayerBody);

		final FixtureDef PLAYER_BODY_FIX = PhysicsFactory.createFixtureDef(10.0f, 1.0f, 0.0f);
		final Body body = PhysicsFactory.createCircleBody(physicsWorld, sPlayerBody, BodyType.DynamicBody, PLAYER_BODY_FIX);
		this.scene.attachChild(sPlayerBody);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(sPlayerBody, body, true, false));
		
		final FixtureDef PLAYER_SWORD_FIX = PhysicsFactory.createFixtureDef(10.0f, 1.0f, 0.0f);
		final Body sword = PhysicsFactory.createCircleBody(physicsWorld, sPlayerBody, BodyType.DynamicBody, PLAYER_SWORD_FIX);
		//final Body sword = PhysicsFactory.createBoxBody(pPhysicsWorld, pAreaShape, pBodyType, pFixtureDef)
		this.scene.attachChild(sPlayerBody);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(sPlayerBody, sword, true, false));

		/* Velocity control (left). */
		final float x1 = 0;
		final float y1 = CAMERA_HEIGHT - this.mOnScreenControlBaseTextureRegion.getHeight();
		final AnalogOnScreenControl velocityOnScreenControl = new AnalogOnScreenControl(x1, y1, this.mCamera, this.mOnScreenControlBaseTextureRegion, this.mOnScreenControlKnobTextureRegion, 0.1f, this.getVertexBufferObjectManager(), new IAnalogOnScreenControlListener()
		{
			@Override
			public void onControlChange(final BaseOnScreenControl pBaseOnScreenControl, final float pValueX, final float pValueY)
			{
				// physicsHandler.setVelocity(pValueX * 100, pValueY *
				// 100);
				body.setLinearVelocity(pValueX * 10, pValueY * 10);
				// mCamera.setMaxVelocity(pValueX * 10, pValueY * 10);

			}

			@Override
			public void onControlClick(final AnalogOnScreenControl pAnalogOnScreenControl)
			{
				/* Nothing. */
			}
		});
		velocityOnScreenControl.getControlBase().setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
		velocityOnScreenControl.getControlBase().setAlpha(0.5f);

		scene.setChildScene(velocityOnScreenControl);

		/* Rotation control (right). */
		final float y2 = y1;
		final float x2 = CAMERA_WIDTH - this.mOnScreenControlBaseTextureRegion.getWidth();
		final AnalogOnScreenControl rotationOnScreenControl = new AnalogOnScreenControl(x2, y2, this.mCamera, this.mOnScreenControlBaseTextureRegion, this.mOnScreenControlKnobTextureRegion, 0.1f, this.getVertexBufferObjectManager(), new IAnalogOnScreenControlListener()
		{
			@Override
			public void onControlChange(final BaseOnScreenControl pBaseOnScreenControl, final float pValueX, final float pValueY)
			{
				if (pValueX == x1 && pValueY == x1)
				{
					sPlayerBody.setRotation(x1);
				} else
				{
					sPlayerBody.setRotation(MathUtils.radToDeg((float) Math.atan2(pValueX, -pValueY)));
				}
			}

			@Override
			public void onControlClick(final AnalogOnScreenControl pAnalogOnScreenControl)
			{
				/* Nothing. */
			}
		});
		rotationOnScreenControl.getControlBase().setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
		rotationOnScreenControl.getControlBase().setAlpha(0.5f);

		velocityOnScreenControl.setChildScene(rotationOnScreenControl);

		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}
}
